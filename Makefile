.PHONY: all clean release build run test test-panic test-st macros

# non-versioned include
VARS ?= vars.mk
-include $(VARS)

CARGO ?= $(shell which cargo)
TARGET := ./target/debug/basis
override CARGO_BUILD_ARGS += --features "$(FEATURES)"

all: build

build:
	$(CARGO) build $(CARGO_BUILD_ARGS)

run:
	$(CARGO) run $(CARGO_BUILD_ARGS)

test:
	$(CARGO) test $(TEST) $(CARGO_BUILD_ARGS) -- --nocapture

test-st:
	$(CARGO) test $(TEST) $(CARGO_BUILD_ARGS) -- --nocapture --test-threads 1

macros:
	$(CARGO) rustc $(CARGO_BUILD_ARGS) -- --pretty=expanded

clean:
	rm -rf target/
	cargo clean

