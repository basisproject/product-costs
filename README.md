# Product costs

An independent prototype of the Basis algorithm that tracks product costs. The original version of this project essentially only tracked labor costs, as an implementation of the pricing mechanisms of Marx's Labor Theory of Value: the cost of anything is the labor that went into making it. While this theory is still useful, it's important to note that it only encompasses part of the picture. In order to truly reach an awareness of our resource usage, it's important to track resources separately from labor. While scarce resources may take more labor to process, thereby raising their "cost," that doesn't tell us how much of that resource is used in production or *how much is left in the earth*.

The idea is to break the costs down, per-raw-material, and associate them with each product. Eventually, the raw materials will be tagged to their sources and consumption rates over estimated amounts of that resource left in the particular source it came from will allow us to get a much larger picture of how much of each resource a product uses.

A chair doesn't cost $17.00, and it doesn't cost 1.5 labor hours: it now costs 1.5 labor hours, 2kg coal, 6kg wood, 3L oil, 2g iron, etc.

This is a rust project that generates the test data and prices, along with a web-based visualization tool (built in react). Much of the data is randomized in order to try and simulate what might be real-world activity.

## Why?

To show that disaggregate tracking of resources into materials not only can be done, but can be done so automatically such that any company using the system would be tracking its full costs.

The idea is that this accounts for externalities much more than hiding everything behind one price. You can now see how much fossil fuels each product uses. You can see how much green energy was used. You can make informed decisions as a producer or consumer about what products you use.

Note that this project assumes a non-market mode of product: building for use. Whether that is a communist gift economy or a socialist economy that pays people/charges for products based on labor, the premise is the same: an economy without profit.

## How?

Essentially, for a given period, take the total disaggregate costs and divide by the outputs =]. Obviously there's a bit more detail. If you're interested in the algorithm, see the `product_cost` function in [the project's main.rs file](https://gitlab.com/basis-/product-costs/blob/master/src/main.rs).

## Run

For the rust portion, simply `make run`.

For react, you'll need node/npm and a web server:

```sh
cd plotter/
npm install
npm run build
```

Now point your browser at `http://127.0.0.1/path/to/product-costs/plotter/build/` (make sure the rust project is running) and be AMAZED.

<img style="max-width: 100%; height: auto;" src="https://gitlab.com/conduct0r/product-costs/raw/master/plotter/public/assets/images/plotter.png">

