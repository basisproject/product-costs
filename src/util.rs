use rand::{self, Rng};

pub fn add_variance(num: f64, percent: f64) -> f64 {
    if percent == 0.0 {
        return num;
    }
    let mut rng = rand::thread_rng();
    num + rng.gen_range(0.0, percent * num)
}

