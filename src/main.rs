mod util;
mod products;

use std::thread;
use std::time::Duration;
use std::sync::{Arc, RwLock};
use std::collections::HashMap;
use exonum::crypto::Hash;
use rand::{self, Rng};
use chrono::{DateTime, Utc, NaiveDateTime};
use uuid;
use serde::Serialize;
use iron::prelude::*;
use iron::status;
use models::order::{Order, ProcessStatus, CostCategory, ProductEntry};
use models::costs::Costs;
use models::amortization::Amortization;
use models::labor::Labor;
use costs;
use crate::products::TestProduct;

// how many workers our company has
static WORKERS: u64 = 4;
// how many things we can build at once
static CONCURRENCY: u64 = 4;
// how many time periods in a cycle
static CYCLE: u64 = 24 * 7;
// how many microseconds per tick (ie, per-hour)
static TIME_SCALE: u64 = 1000 * 10;
// how much tracking data to keep around (used for predictive pricing)
static MA_POINTS: usize = 52;
// how many cycle's worth of inventory for have on-hand
static MIN_INVENTORY_FACTOR: u64 = 2;
// when purchasing more inventory, how many cycle's worth to buy
static INVENTORY_CYCLE_FACTOR: u64 = 4;

#[derive(Debug, Clone)]
struct WrappedOrder {
    order: Order,
    ty: TestProduct,
    remaining: u64,
}

impl WrappedOrder {
    pub fn new(order: &Order, ty: &TestProduct) -> Self {
        Self {
            order: order.clone(),
            ty: ty.clone(),
            remaining: order.products[0].quantity as u64,
        }
    }
}

#[derive(Debug, Default, Clone, Serialize)]
struct Tracker {
    costs: HashMap<String, Costs>,
    outputs: HashMap<String, u64>,
    product_costs: HashMap<String, Costs>,
}

#[derive(Debug, Clone)]
struct Company {
    workers: u64,
    concurrency: u64,
    orders_incoming: Vec<Order>,
    orders_outgoing: Vec<Order>,
    orders_current: Vec<WrappedOrder>,
    labor: Vec<Labor>,
    start: DateTime<Utc>,
    hours: u64,
    amortization: HashMap<String, Amortization>,
    outputs: HashMap<String, u64>,
    track: Vec<Tracker>,
    inventory: HashMap<String, f64>,
}

impl Company {
    pub fn next_order<'a>(&'a mut self) -> Option<&'a mut WrappedOrder> {
        if self.orders_current.len() == 0 {
            return None;
        }
        let order = &self.orders_current[0];
        if order.remaining == 0 {
            self.orders_current.remove(0);
            return self.next_order();
        }
        Some(&mut self.orders_current[0])
    }

    pub fn track_output(&mut self, prod: &TestProduct, amount: u64) {
        let current = self.outputs.get(prod.name()).unwrap_or(&0).clone();
        self.outputs.insert(prod.name().to_string(), current + amount);
    }

    pub fn now(&self) -> DateTime<Utc> {
        self.now_plus(0)
    }

    pub fn now_plus(&self, seconds: i64) -> DateTime<Utc> {
        DateTime::<Utc>::from_utc(NaiveDateTime::from_timestamp(self.start.timestamp() + ((self.hours * 3600) as i64) + (seconds as i64), 0), Utc)
    }

    pub fn trim(&mut self) {
        let now = self.now();
        self.orders_incoming.retain(|o| (now - o.updated).num_days() < 365);
        self.orders_outgoing.retain(|o| (now - o.updated).num_days() < 365);
        self.labor.retain(|l| (now - l.end).num_days() < 365);
    }
}

pub fn gen_uuid() -> String {
    format!("{}", uuid::Uuid::new_v4())
}

fn outgoing_order_from_testproduct(prod: &TestProduct, amount: f64, date: &DateTime<Utc>) -> Order {
    let hash = Hash::new([1, 28, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4]);
    let mut inp_costs = products::input_costs(&prod, 0.1);
    inp_costs.track_labor(util::add_variance(prod.effort(), 0.1));
    let order = Order::new(
        gen_uuid().as_str(),
        "company1",
        gen_uuid().as_str(),
        &CostCategory::Inventory,
        &vec![ProductEntry::new(prod.id().as_str(), amount, &inp_costs)],
        &ProcessStatus::Finalized,
        &date,
        &date,
        0,
        &hash
    );
    order
}

fn incoming_order_from_testproduct(prod: &TestProduct, amount: f64, date: &DateTime<Utc>) -> Order {
    let mut order = outgoing_order_from_testproduct(prod, amount, date);
    order.company_id_from = order.company_id_to.clone();
    order.company_id_to = "company1".to_string();
    order.process_status = ProcessStatus::Accepted;
    order
}

fn inventory(company: Arc<RwLock<Company>>, initial: bool) {
    let min = {
        let comp = company.read().unwrap();
        let mut min = HashMap::new();
        for prod in &TestProduct::list() {
            for (input, count) in prod.inputs() {
                let current = min.get(&input).unwrap_or(&0.0).clone();
                let per_hour = (comp.concurrency as f64) * (count / prod.effort());
                min.insert(input, current + per_hour);
            }
        }
        drop(comp);
        min
    };

    for (input, count) in min.iter() {
        let period_min = (CYCLE as f64) * count;
        let inp_min = period_min * (MIN_INVENTORY_FACTOR as f64);
        let inp_ideal = inp_min + ((INVENTORY_CYCLE_FACTOR as f64) * period_min);
        let mut comp = company.write().unwrap();
        let current_inv = comp.inventory.get(input.name()).unwrap_or(&0.0) + 0.0;
        if current_inv < inp_min {
            let purchase_num = inp_ideal - current_inv;
            let mut inp_costs = products::input_costs(&input, 0.1);
            inp_costs.track_labor(util::add_variance(input.effort(), 0.1));
            let order = outgoing_order_from_testproduct(&input, purchase_num as f64, &comp.now());
            if initial {
                // TODO: set wamortization on order?
                comp.orders_outgoing.push(order);
            } else {
                comp.orders_outgoing.push(order);
            }
            comp.inventory.insert(input.name().to_string(), current_inv + purchase_num);
        }
        drop(comp);
    }
}

fn operating(company: Arc<RwLock<Company>>) {
    // non-inventory purchasing
    let mut comp = company.write().unwrap();
    let mut rng = rand::thread_rng();
    let electricity_used = rng.gen_range(10, 30);
    let input = TestProduct::Electricity;
    let mut order = outgoing_order_from_testproduct(&input, electricity_used as f64, &comp.now());
    order.cost_category = CostCategory::Operating;
    comp.orders_outgoing.push(order);
    if rng.gen_range(0.0, 1.0) < 0.01 {
        let purchase_num = rng.gen_range(10, 30);
        let input = TestProduct::Widget;
        let mut order = outgoing_order_from_testproduct(&input, purchase_num as f64, &comp.now());
        order.cost_category = CostCategory::Operating;
        comp.orders_outgoing.push(order);
    }
}

fn builder_thread(_id: u64, company: Arc<RwLock<Company>>) {
    loop {
        let order = {
            let mut comp = company.write().unwrap();
            comp.next_order()
                .map(|o| {
                    o.remaining -= 1;
                    o.clone()
                })
        };
        let order = match order {
            Some(x) => x,
            None => {
                thread::sleep(Duration::from_micros(TIME_SCALE));
                continue;
            },
        };
        {
            let mut comp = company.write().unwrap();
            for (inp, count) in order.ty.inputs() {
                let cur_inv = comp.inventory.get(inp.name()).unwrap_or(&0.0) + 0.0;
                comp.inventory.insert(inp.name().to_string(), cur_inv - count);
            }
        }
        let production_hours = util::add_variance(order.ty.effort() as f64, 0.3);
        thread::sleep(Duration::from_micros((production_hours * (TIME_SCALE as f64)) as u64));
        {
            let mut comp = company.write().unwrap();
            comp.track_output(&order.ty, 1);
        }
        thread::sleep(Duration::from_micros((0.1 * (TIME_SCALE as f64)) as u64));
    }
}

fn plot(company: Arc<RwLock<Company>>) {
    Iron::new(move |_: &mut Request| {
        let comp = company.read().unwrap();
        let json = serde_json::to_string(&comp.track).unwrap();
        let mut res = Response::with((status::Ok, json));
        res.headers.set_raw("Access-Control-Allow-Origin", vec![String::from("*").into_bytes()]);
        Ok(res)
    }).http("localhost:6969").unwrap();
}

fn tick(company: Arc<RwLock<Company>>) {
    let mut comp = company.write().unwrap();
    comp.hours += 1;
    for x in 0..comp.workers {
        let start = comp.now_plus(-3600);
        let end = comp.now();
        let hash = Hash::new([1, 28, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4]);
        let labor = Labor::new(
            gen_uuid().as_str(),
            "company1",
            format!("worker-{}", x).as_str(),
            Some(&start),
            Some(&end),
            &end,
            &end,
            0,
            &hash
        );
        comp.labor.push(labor);
    }
    if comp.hours % 8 == 0 {
        comp.hours += 16;
    }
    if comp.hours % CYCLE == 0 {
        let product_types = TestProduct::list();
        let mut trackdata: Tracker = Default::default();
        for prod in &product_types {
            let count = comp.outputs.get(prod.name()).unwrap_or(&0);
            trackdata.outputs.insert(prod.name().to_string(), *count);
        }
        let mut product_map = HashMap::new();
        for testprod in &product_types {
            product_map.insert(testprod.id(), testprod.to_product());
        }

        // determine this weeks costs (for the tracker, mainly)
        let mut costs = HashMap::new();
        {
            let mut costs_operating = Costs::new();
            let mut costs_inventory = Costs::new();
            let beginning_of_cycle = comp.now_plus(-3600 * CYCLE as i64);
            for labor in &comp.labor {
                if labor.end >= beginning_of_cycle {
                    costs_operating.track_labor(labor.hours());
                }
            }
            for order in &comp.orders_outgoing {
                if order.updated >= beginning_of_cycle {
                    for prod in &order.products {
                        match order.cost_category {
                            CostCategory::Operating => {
                                costs_operating = costs_operating + (prod.costs.clone() * prod.quantity);
                            }
                            CostCategory::Inventory => {
                                costs_inventory = costs_inventory + (prod.costs.clone() * prod.quantity);
                            }
                            _ => {}
                        }
                    }
                }
            }
            costs.insert(String::from("operating"), costs_operating);
            costs.insert(String::from("inventory"), costs_inventory);
        }

        println!("--- costs {:?}", costs);
        comp.outputs.clear();
        trackdata.costs = costs;
        comp.track.insert(0, trackdata);
        comp.track.truncate(MA_POINTS);
        comp.track[0].product_costs = costs::calculate_costs(
            &comp.orders_incoming,
            &comp.orders_outgoing,
            &comp.labor,
            &comp.amortization,
            &product_map
        ).unwrap();
    }
}

fn economy(company: Arc<RwLock<Company>>) {
    let mut comp = company.write().unwrap();
    let mut rng = rand::thread_rng();
    if comp.orders_current.len() < (CONCURRENCY * 4) as usize {
    //if rng.gen_range(0.0, 1.0) < 0.01 {
        let product_list = TestProduct::list();
        let product = product_list[rng.gen_range(0, product_list.len())].clone();
        let count = rng.gen_range(6, 12);
        let order = incoming_order_from_testproduct(&product, count as f64, &comp.now());
        let wrapped = WrappedOrder::new(&order, &product);
        comp.orders_incoming.push(order);
        comp.orders_current.push(wrapped);
    }
}

fn trim(company: Arc<RwLock<Company>>) {
    let mut comp = company.write().unwrap();
    comp.trim();
}

fn main() {
    let company: Arc<RwLock<Company>> = Arc::new(RwLock::new(Company {
        workers: WORKERS,
        concurrency: CONCURRENCY,
        orders_incoming: Default::default(),
        orders_outgoing: Default::default(),
        orders_current: Default::default(),
        labor: Default::default(),
        start: Utc::now(),
        hours: 0,
        amortization: HashMap::new(),
        outputs: Default::default(),
        track: Default::default(),
        inventory: Default::default(),
    }));
    let mut handles = vec![];
    println!("--- so it begins ---");
    for i in 0..company.write().unwrap().concurrency {
        let company_clone = company.clone();
        let id = i + 1;
        handles.push(thread::Builder::new().name(format!("builder-{}", id)).spawn(move || {
            thread::sleep(Duration::from_micros(TIME_SCALE));
            builder_thread(id, company_clone);
        }).expect("failed to spawn builder thread"));
    }
    let company_clone1 = company.clone();
    handles.push(thread::Builder::new().name("stats-http".to_owned()).spawn(move || {
        plot(company_clone1);
    }).expect("failed to spawn http stats thread"));
    inventory(company.clone(), true);
    loop {
        inventory(company.clone(), false);
        operating(company.clone());
        tick(company.clone());
        economy(company.clone());
        trim(company.clone());
        thread::sleep(Duration::from_micros(TIME_SCALE));
    }
}

