use serde::Serialize;
use exonum::crypto::Hash;
use chrono::{DateTime, Utc};
use models::product::{Product, Unit, Dimensions, Input, EffortTime, Effort};
use models::costs::Costs;
use crate::util;

#[derive(Debug, Clone, PartialEq, Eq, Hash, Serialize)]
pub enum TestProduct {
    #[serde(rename = "oil")]
    Oil,
    #[serde(rename = "coal")]
    Coal,
    #[serde(rename = "iron")]
    Iron,
    #[serde(rename = "electricity")]
    Electricity,
    #[serde(rename = "steel")]
    Steel,
    #[serde(rename = "widget")]
    Widget,
    #[serde(rename = "pipe")]
    Pipe,
    #[serde(rename = "wheel")]
    Wheel,
    #[serde(rename = "lever")]
    Lever,
    ChairSimple,
    ChairExecutive,
    ChairAeron,
}

impl TestProduct {
    pub fn is_raw(&self) -> bool {
        match self {
            TestProduct::Oil => true,
            TestProduct::Coal => true,
            TestProduct::Iron => true,
            _ => false,
        }
    }

    pub fn inputs(&self) -> Vec<(TestProduct, f64)> {
        match self {
            TestProduct::Oil => vec![],
            TestProduct::Coal => vec![],
            TestProduct::Iron => vec![],
            TestProduct::Electricity => vec![
                (TestProduct::Coal, 5.1),
                (TestProduct::Oil, 1.2),
            ],
            TestProduct::Steel => vec![
                (TestProduct::Iron, 2.1),
                (TestProduct::Oil, 0.3),
            ],
            TestProduct::Widget => vec![
                //(TestProduct::Oil, 0.1),
                //(TestProduct::Steel, 4.7),
                (TestProduct::Steel, 2.0),
            ],
            TestProduct::Pipe => vec![
                (TestProduct::Oil, 3.2),
                (TestProduct::Steel, 1.4),
            ],
            TestProduct::Wheel => vec![
                (TestProduct::Widget, 1.0)
            ],
            TestProduct::Lever => vec![
                (TestProduct::Pipe, 1.0)
            ],
            TestProduct::ChairSimple => vec![
                (TestProduct::Widget, 1.0)
            ],
            TestProduct::ChairExecutive => vec![
                (TestProduct::Widget, 2.0),
                (TestProduct::Wheel, 4.0)
            ],
            TestProduct::ChairAeron => vec![
                (TestProduct::ChairExecutive, 1.0),
                (TestProduct::Widget, 4.0),
                (TestProduct::Wheel, 4.0),
                (TestProduct::Lever, 2.0)
            ],
        }
    }

    pub fn effort(&self) -> f64 {
        match self {
            TestProduct::Oil => 0.2,
            TestProduct::Coal => 3.2,
            TestProduct::Iron => 1.2,
            TestProduct::Electricity => 0.01,
            TestProduct::Steel => 2.3,
            TestProduct::Widget => 1.0,
            TestProduct::Pipe => 2.0,
            TestProduct::Wheel => 1.0,
            TestProduct::Lever => 1.0,
            TestProduct::ChairSimple => 1.0,
            TestProduct::ChairExecutive => 4.0,
            TestProduct::ChairAeron => 13.0,
        }
    }

    pub fn name(&self) -> &'static str {
        match self {
            TestProduct::Oil => "oil",
            TestProduct::Iron => "iron",
            TestProduct::Coal => "coal",
            TestProduct::Electricity => "electricity",
            TestProduct::Steel => "steel",
            TestProduct::Widget => "widget",
            TestProduct::Pipe => "pipe",
            TestProduct::Wheel => "wheel",
            TestProduct::Lever => "lever",
            TestProduct::ChairSimple => "chair_simple",
            TestProduct::ChairExecutive => "chair_executive",
            TestProduct::ChairAeron => "chair_aeron",
        }
    }

    pub fn id(&self) -> String {
        format!("{}", self.name())
    }

    pub fn to_product(&self) -> Product {
        let product_id = self.id();
        let company_id = "cbea8c0e-0b2f-4e93-9f87-108180b512d8";
        let effort = Effort::new(&EffortTime::Seconds, (self.effort() * 3600.0) as u64);
        let unit = match self {
            TestProduct::Oil => Unit::Milliliter,
            TestProduct::Electricity => Unit::WattHour,
            _ => Unit::Millimeter,
        };
        let dimensions = match self {
            TestProduct::Oil => Dimensions::new(0.0, 0.0, 0.0),
            _ => Dimensions::new(10.0, 10.0, 10.0),
        };
        let inputs = self.inputs().iter()
            .map(|(inp, count)| Input::new(&inp.to_product().id, *count))
            .collect::<Vec<_>>();
        let created: DateTime<Utc> = "2019-09-28T00:00:00Z".parse().unwrap();
        let hash = Hash::new([1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4, 1, 27, 6, 4]);
        Product::new(product_id.as_str(), company_id, self.name(), &unit, 1000.0, &dimensions, &inputs, &effort, true, "", &created, &created, None, 0, &hash)
    }

    // the products our company makes specifically
    pub fn list() -> Vec<TestProduct> {
        vec![TestProduct::ChairSimple, TestProduct::ChairExecutive, TestProduct::ChairAeron]
    }
}

pub fn input_costs(prod: &TestProduct, variance: f64) -> Costs {
    let mut costs = Costs::new();
    for (inp, count) in prod.inputs() {
        let mut inp_costs = input_costs(&inp, variance) * count;
        inp_costs.track_labor(util::add_variance(inp.effort(), variance) * count);
        if inp.is_raw() {
            inp_costs.track(&inp.id(), count);
        }
        costs = costs + inp_costs;
    }
    costs
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::products::TestProduct;

    #[test]
    fn calc_input_costs() {
        let costs1 = input_costs(&TestProduct::Widget, 0.0);
        assert_eq!(costs1.labor(), 9.76);
        assert_eq!(costs1.products().get(&TestProduct::Oil.id()).unwrap(), &0.6);
        assert_eq!(costs1.products().get(&TestProduct::Iron.id()).unwrap(), &4.2);
    }
}

