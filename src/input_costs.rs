use models::costs::Costs;
use crate::products::TestProduct;
use crate::util;

pub fn input_costs(prod: &TestProduct, variance: f64) -> Costs {
    let mut costs = Costs::new();
    for (inp, count) in prod.inputs() {
        let mut inp_costs = input_costs(&inp, variance) * count;
        inp_costs.track_labor(util::add_variance(inp.effort(), variance) * count);
        if inp.is_raw() {
            inp_costs.track(&inp.id(), count);
        }
        costs = costs + inp_costs;
    }
    costs
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::products::TestProduct;

    #[test]
    fn calc_input_costs() {
        let costs1 = input_costs(&TestProduct::Widget, 0.0);
        assert_eq!(costs1.labor(), 9.76);
        assert_eq!(costs1.products().get(&TestProduct::Oil.id()).unwrap(), &0.6);
        assert_eq!(costs1.products().get(&TestProduct::Iron.id()).unwrap(), &4.2);
    }
}

