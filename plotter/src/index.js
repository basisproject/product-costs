import React from 'react';
import ReactDOM from 'react-dom';
import { LineChart } from 'react-chartkick'
import 'chart.js'

import './app.css';

function rounder(num, decimals = 3) {
	const mul = Math.pow(10, decimals);
	return Math.round(num * mul) / mul;
}

class StatsPlotter extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			pause: false,
			track: [],
		};
		setInterval(function() {
			if(this.state.pause) return;
			const opts = {
				method: 'get',
				url: 'http://localhost:6969/',
			};
			window.Sexhr(opts)
				.then(function([res, xhr]) {
					this.setState({track: JSON.parse(res)});
				}.bind(this))
				.catch(function(err) {
					console.error('err: ', err);
				});
		}.bind(this), 500);
	}

	toggle_pause() {
		this.setState({pause: !this.state.pause});
	}

	render() {
		const chartopts = {
			pointRadius: 0,
			lineTension: 0,
		};
		function flatten_costs(tracks) {
			return tracks.map((t) => {
				Object.keys(t.costs).forEach((cat) => {
					t.costs[cat] = Object.assign({labor: t.costs[cat].labor}, t.costs[cat].products);
				});
				Object.keys(t.product_costs).forEach((c) => {
					t.product_costs[c] = Object.assign({labor: t.product_costs[c].labor}, t.product_costs[c].products);
				});
				return t;
			});
		}
		const trackcopy = JSON.parse(JSON.stringify(this.state.track));
		const trackdata = flatten_costs(trackcopy.reverse());
		const min = 0;
		const max = trackdata.length;
		const current = trackdata[trackdata.length - 1] || {};
		const product_costs = current.product_costs || {};
		const products = Object.keys(product_costs)
			.sort((a, b) => a.localeCompare(b));
		const cost_categories = {};
		const cost_types = {};
		trackdata.forEach((t) => {
			Object.keys(t.costs).forEach((c) => {
				cost_categories[c] = true;
				Object.keys(t.costs[c]).forEach((t) => cost_types[t] = true);
			});
		});

		const costsdata = {};
		Object.keys(cost_categories).sort().forEach((ctype) => {
			costsdata[ctype] = Object.keys(cost_types).sort().map((type) => {
				return {
					name: `${type}`,
					data: trackdata.map((track, i) => [i, (track.costs[ctype] || {})[type] || 0]),
				};
			});
		});

		const productcosts = {};
		products.forEach((prod) => {
			productcosts[prod] = Object.keys(cost_types).sort().map((type) => {
				return {
					name: `${type}`,
					data: trackdata.map((track, i) => [i, (track.product_costs[prod] || {})[type] || 0]),
				};
			});
		});

		const outputs = products.map((type) => {
			return {
				name: `${type} outputs`,
				data: trackdata.map((track, i) => [i, track.outputs[type] || 0]),
			};
		});

		return (
			<div className="company-chart">
				<div class="stats">
					<h1>
						Merman Hiller
						&nbsp;
						<button type="button" onClick={() => {this.toggle_pause()}}>{this.state.pause ? 'Resume' : 'Pause'}</button>
					</h1>
					<table>
						<thead>
							<tr>
								{Object.keys(cost_categories).sort().map((cat) => (
									<th colspan="2">Costs {cat}</th>
								))}
							</tr>
						</thead>
						<tbody>
							{Object.keys(cost_types).sort().map((type) => {
								return (
									<tr>
										{Object.keys(cost_categories).sort().map((cat) => [
											<td>{type}</td>,
											<td class="alignright">{rounder((current.costs[cat] || {})[type] || 0)}</td>
										])}
									</tr>
								);
							})}
						</tbody>
					</table>

					<h2>Product costs</h2>
					<table>
						<thead>
							<tr>
								{products.map((type) => (
									<th colspan="2">{type}</th>
								))}
							</tr>
						</thead>
						<tbody>
							{Object.keys(cost_types).sort().map((type) => {
								return (
									<tr>
										{products.map((ptype) => [
											<td>{type}</td>,
											<td class="alignright">{rounder((product_costs[ptype] || {})[type] || 0)}</td>,
										])}
									</tr>
								);
							})}
						</tbody>
					</table>
				</div>
				{Object.keys(costsdata).map((type) => {
					return [
						<h2>Costs {type}</h2>,
						<LineChart data={costsdata[type]} dataset={chartopts} width="100%" height={200} xmin={min} xmax={max} redraw={false} />,
					];
				})}
				<h2>Product outputs</h2>
				<LineChart data={outputs} dataset={chartopts} width="100%" height={200} xmin={min} xmax={max} redraw={false} />

				{products.map((prod) => {
					return [
						<h2>Product {prod} costs</h2>,
						<LineChart data={productcosts[prod]} dataset={chartopts} width="100%" height={200} xmin={min} xmax={max} redraw={false} />,
					];
				})}
			</div>
		);
	}
}

ReactDOM.render(
	<StatsPlotter />,
	document.getElementById('root')
);

